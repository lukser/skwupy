# Skwupy

A flexible and highly modular Discord bot. It tries to solve the lack of fully-fledged bots available on the internet. Unlike other bots, it is highly configurable, efficient, and allows self-hosting without paying for premium subscription.

## Quick Start

In order to run bot as a Docker service:

    % git clone https://gitlab.com/lukser/skwupy.git
    % cd skwupy
    % docker compose up -d --build

## Features

Here is a list of features this bot has:

- Open-source - free as in freedom
- Modularity & Flexibility - low cohesive code and maintainability.
- Lightweight - no all-in-one, though possible.
- Easy to use - self-hosting is easy.

## FAQ

- Why Skwupy if X already exists?

Skwupy is open-source, allows easy self-hosting and flexible enough to add as many features as you want.
